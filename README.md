# ngx-lib-template

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.0.

It consist of 3 ng libraries ngx-lib-commons, ngx-lib-styles and ngx-lib-material.

## Development server

`git clone https://HRTGXC@bitbucket.org/plantandfood/ngx-lib-template.git`  
### Before running `npm install`, temporary remove these three lines fron `package.json` file.

    "ngx-lib-material": "file:projects/ngx-lib-material/dist",
    "ngx-lib-styles": "file:projects/ngx-lib-styles/dist",
    "ngx-lib-common": "file:projects/ngx-lib-common/dist",

 **Note** : Common should be build after styles and material.  
`npm run build-styles`  
`npm run build-material`  

### Bring back those first 2 lines and do `npm install`.

`npm run build-common`
### Bring back those first 1 lines and do `npm install`.


Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

ngx-lib-common : `npm run build-common`  
ngx-lib-styles : `npm run build-styles`  
ngx-lib-material : `npm run build-material`

## Publish build pkg to (nexus, npm-registry etc) using `.npmrc`

ngx-lib-common : `npm run publish-common`  
ngx-lib-styles : `npm run publish-styles`  
ngx-lib-material : `npm run publish-material`

## Git push changes for individial libs

`git add files`  
`git commit -m "Message here"`  
Push to ngx-lib-common: `git subtree push --prefix projects/ngx-lib-common ngx-lib-common [branch name]`  
Push to ngx-lib-material: `git subtree push --prefix projects/ngx-lib-material ngx-lib-material [branch name]`  
Push to ngx-lib-styles: `git subtree push --prefix projects/ngx-lib-styles ngx-lib-styles [branch name]`


## Initial Commit for git subtress (In case of  adding a new library)

### create a new repo for subtree (library) and go to project folder.
`git init`  
`git add .`  
`git commit -m "Initial Commit"`  
After pushing the changes delete the child library from project and connect it via git subtree using a cmd below  
`git subtree add --prefix projects/ngx-lib-common  https://HRTGXC@bitbucket.org/plantandfood/ngx-lib-common.git master`


## Git push changes to subtree (means to child project)

### ngx-lib-material
`git subtree push --prefix projects/ngx-lib-material https://HRTGXC@bitbucket.org/plantandfood/ngx-lib-material.git  master`

### ngx-lib-common

`git subtree push --prefix projects/ngx-lib-common https://HRTGXC@bitbucket.org/plantandfood/ngx-lib-common.git  master`

### ngx-lib-styles
`git subtree push --prefix projects/ngx-lib-styles https://HRTGXC@bitbucket.org/plantandfood/ngx-lib-styles.git  master`
