import { OnDestroy } from '@angular/core';
import { PageEvent, Sort } from '@angular/material';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { flatten } from 'flat';
import { cloneDeep, isEmpty, omitBy } from 'lodash';
import { BehaviorSubject, Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { FilterContext } from './filter-context';
import { FilterField } from './filter-field';
import { FilterPanelService } from './filter-panel-service';
import { FilterPageAdapter } from './filtered-page-adapter';
import { FilteredQueryDependencies } from './filtered-query-dependencies';
import { FilteredQueryResult } from './filtered-query-result';
import { Disposables } from './disposables';
import { PageState } from './page-state';

export abstract class FilteredQueryCoordinator<TFilterModel, TQueryModel, TData>
  implements OnDestroy {
  private _queryVersion: number = 0;
  private _queryModelChanged = new BehaviorSubject<TQueryModel>(undefined);
  private _queryModel: any = undefined;
  private _data = new BehaviorSubject<TData[]>([]);
  private _pageChanged = new BehaviorSubject<PageState>(null);
  private _filterContext: FilterContext<TFilterModel>;
  private _router: Router;
  private _route: ActivatedRoute;
  protected readonly disposables = new Disposables();
  readonly data$: Observable<TData[]>;
  readonly pageChanged$: Observable<PageState>;
  readonly queryModelChanged$: Observable<TQueryModel>;
  preventFocus: boolean = false;

  /**
   * Returns the FilterContext associated with this FilteredQueryCoordinator instance.
   * The FilterContext is not available until the init() method has been invoked.
   */
  get filterContext(): FilterContext<TFilterModel> {
    return this._filterContext;
  }

  constructor(
    public readonly filterPanelService: FilterPanelService,
    private pageAdapter: FilterPageAdapter,
    private filterFields: FilterField[]
  ) {
    let deps: FilteredQueryDependencies = (<any>(
      filterPanelService
    )).__getDependencies();
    this._route = deps.activatedRoute;
    this._router = deps.router;
    this.data$ = this._data.asObservable();
    this.pageChanged$ = this._pageChanged.asObservable();
    this.queryModelChanged$ = this._queryModelChanged.asObservable();
  }

  ngOnDestroy() {
    this.disposables.dispose();
  }

  /**
   * When implemented in a derived class executes a query to return filtered data as a RestResponse.
   * @param params The filter parameters.
   */
  protected abstract executeQuery(
    params?: Params
  ): Promise<FilteredQueryResult<TData>>;
  /**
   * When implemented in a derived class converts the RestResponse returned by the executeQuery function to an array of type TData.
   */
  // protected abstract mapResponse(response: RestResponse): TData[];
  /**
   * When implemented in a derived class returns the Formly form configuration data as either a literal array or as a factory method which returns a literal array.
   */

  protected abstract createFormDefinition():
    | FormlyFieldConfig[]
    | (() => FormlyFieldConfig[]);

  /**
   * Creates the FilterContext associated with this FilteredQueryCoordinator instance and invokes its focus method.
   * @param suppressFocus If true, unfocuses any pre-existing active FilterContext and does not give focus to the context associated with this FilteredQueryCoordinator.
   */
  async init(suppressFocus: boolean = false) {
    if (this._filterContext) throw new Error('init() has already been called.');

    let filterModel: any;
    await this._route.queryParams
      .pipe(first())
      .toPromise()
      .then(p => {
        filterModel = reconstructFilterModel(this.filterFields, p);
      });

    this._filterContext = this.filterPanelService.createContext(
      this.createFormDefinition(),
      filterModel
    );
    this.onAfterInit();
    // If the Formly configuration data has been returned as a factory method, then it will be instantiated the first time
    // that the focus() method is invoked. This enables Formly form configuration elements to reference the FilterContext
    // if they need to.
    if (!suppressFocus) {
      this._filterContext.focus();
    } else {
      this.filterPanelService.clearFocus();
    }

    this.subscribeToFilterChange();
  }

  /**
   * Invoked after the init() function has been called. The FilterContext is not available until this point.
   */
  protected onAfterInit() {}

  /**
   * When overridden in a derived class, returns a translated version of the model to be used to generate the parameters passed to the executeQuery function.
   * Supports use cases where the query model is necessarily different to the filter model.
   * @param model The model as it exists in the active filter context.
   */
  protected toQueryModel(model: TFilterModel): TQueryModel {
    return <any>model;
  }

  onSortChange(sort: Sort) {
    let queryParams = this.getQueryParams(true, {
      sort: `${sort.active},${sort.direction}`
    });
    this.loadData(queryParams);
  }

  onPageChange(page: PageEvent) {
    let pageState = {};
    this.pageAdapter.applyState(pageState, page);
    let queryParams = this.getQueryParams(false, pageState);
    this.loadData(queryParams);
  }

  /**
   * Returns the query string parameters for the filter model only, with paging and other parameters omitted.
   */
  getQueryModelParams(): Params {
    if (!this._queryModel) return {};
    return omitBy(
      flatten<Params, {}>(cloneDeep(this._queryModel), { safe: true }),
      isEmpty
    );
  }

  private loadData(params?: Params) {
    let version = ++this._queryVersion;
    this.filterContext.isBusy = true;
    this.executeQuery(params)
      .then(response => {
        this.filterContext.isBusy = false;
        // Don't raise a data changed event if the result is for a request made earlier than the most recent response.
        if (version >= this._queryVersion) {
          this._queryVersion = version;
          this._data.next(response.data);
          this._pageChanged.next(response.page);
        }
      })
      .catch(e => {
        this.filterContext.isBusy = false;
        throw e;
      });
  }

  private subscribeToFilterChange() {
    this.disposables.subscribeTo(
      this._filterContext.modelChanged$,
      (model: TFilterModel) => {
        this._queryModel = this.toQueryModel(model);
        this._queryModelChanged.next(this._queryModel);
        // We don't want to reset the page on the first (init) event, as we may be restoring previous page state from query-string params.
        const resetPage = this._queryVersion > 0;
        let queryParams = this.getQueryParams(resetPage);
        this.loadData(queryParams);
      }
    );
  }

  private getQueryParams(resetPage: boolean, additionalParams?: any): any {
    let queryParams = {
      ...this._route.snapshot.queryParams,
      ...this._queryModel,
      ...additionalParams
    };

    if (resetPage) this.pageAdapter.resetPageIndex(queryParams);
    queryParams = omitBy(
      flatten<Params, {}>(queryParams, { safe: true }),
      isEmpty
    );

    this.updateQueryParams(queryParams);
    return queryParams;
  }

  private updateQueryParams(queryParams: object): void {
    // Clear the query params
    this._router.navigate([], {
      relativeTo: this._route,
      queryParams: null
    });

    this._router.navigate([], {
      relativeTo: this._route,
      queryParams
    });
  }
}

/**
 * Constructs a filter model instance from its field definitions and assigns values from query-string parameters.
 */
function reconstructFilterModel(fields: FilterField[], query: Params): any {
  let model = {};
  fields.forEach(field => {
    ensureFilterField(field, model, query);
  });

  return model;
}

/**
 * Assigns a filter field value to a filter model instance from query-string parameters, and ensures that the property value is in the correct
 * format (string if single-value, array if multi-value).
 */
function ensureFilterField(
  field: FilterField,
  model: any,
  query: Params
): void {
  let i = 0;
  let path = field.path;
  let target = model;
  for (; i < path.length - 1; i++) {
    target = model[path[i]];
    if (target == undefined) {
      target = {};
      model[path[i]] = target;
    }
  }

  let value = query[field.key];
  if (value == undefined) {
    value = field.multiValue ? [] : undefined;
  } else if (field.multiValue) {
    if (typeof value === 'string') {
      value = [value];
    }
  } else if (Array.isArray(value)) {
    value = value.length > 0 ? value[0] : '';
  }

  target[path[i]] = value;
}
