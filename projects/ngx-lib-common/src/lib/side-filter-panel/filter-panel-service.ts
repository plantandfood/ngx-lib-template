import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { BehaviorSubject } from 'rxjs';
import { FilterContext } from './filter-context';
import { FilterPanelController } from './filter-panel-controller';
import { FilteredQueryDependencies } from './filtered-query-dependencies';
import { ViewFilterContext } from './view-filter-context';

/**
 * Represents the internal extended view of a FilterContext/
 */
interface InternalFilterContext<TModel> extends FilterContext<TModel> {
  _raiseModelChanged(): void;
  _formlyConfig: FormlyFieldConfig[];
}

/**
 * A service that provides contextual access to a global filter form and exposes a model representing query filter parameters
 * specified through the form's user-interface.
 */
@Injectable({ providedIn: 'root' })
export class FilterPanelService {
  private _filterPanel: FilterPanelController;
  private _currentContext: InternalFilterContext<any> = undefined;
  private _isOpen: boolean;

  constructor(private router: Router, private route: ActivatedRoute) {}

  /**
   * Gets or sets the opened state of the global FilterPanelComponent.
   * Note that the opened state is different from the visible state. Even though isOpen may be true the FilterPanelComponent will be hidden if
   * there is no active context. This allows the FilterPanelComponent to automatically resume an opened state when the user navigates to a
   * view with an active filter, but to hide itself when no active filter exists.
   */
  get isOpen(): boolean {
    return this._isOpen === true;
  }

  set isOpen(value: boolean) {
    this._isOpen = value === true;
  }

  /**
   * Returns true if the global FilterPanelComponent is visible, otherwise false.
   * The FilterPanelComponent is visible if the opened state is true and an active client FilterPanelContext has focus.
   */
  get isVisible(): boolean {
    return this.isOpen && this.hasContext;
  }

  /**
   * Returns true if an active FilterContext exists, otherwise returns false.
   */
  get hasContext(): boolean {
    return this._currentContext != undefined;
  }

  /**
   * Returns true if an active FilterContext has one or more filter properties applied, otherwise returns false.
   */
  get hasFilter(): boolean {
    return this._filterPanel.hasFilter;
  }

  /**
   * Unfocuses any active filter context.
   */
  clearFocus(): void {
    this.changeContext(undefined);
  }

  /**
   * Creates new FilterContext instance to be used by a consumer of the FilterPanelService service.
   * @param formlyConfig The Formly form definition used to construct the filter panel form for this context.
   */
  createContext<TModel>(
    formlyConfig: FormlyFieldConfig[] | (() => FormlyFieldConfig[]),
    model: TModel = <TModel>{}
  ): FilterContext<TModel> {
    // Javascript can have contextual problems with "this" when using closure;
    // use the "that = this" convention to maintain a reliable reference to the current instance.
    let that = this;
    let _modelChanged: BehaviorSubject<any> = new BehaviorSubject(model);
    let _isBusy = false;
    let _isDisabled = false;
    let _formDef: FormlyFieldConfig[] = undefined;

    let context: InternalFilterContext<TModel> = {
      model: model,
      modelChanged$: _modelChanged.asObservable(),
      get hasFocus() {
        return that._currentContext === context;
      },
      get hasFilter() {
        return that._currentContext === context && that._filterPanel.hasFilter;
      },
      get isBusy() {
        return _isBusy;
      },
      set isBusy(value: boolean) {
        value = value === true;
        if (_isBusy !== value) {
          _isBusy = value;
          if (that._filterPanel) that._filterPanel.applyBusyState(value);
        }
      },
      get isDisabled() {
        return _isDisabled;
      },
      set isDisabled(value: boolean) {
        value = value === true;
        if (_isDisabled !== value) {
          _isDisabled = value;
          if (that._filterPanel) that._filterPanel.applyDisabledState(value);
        }
      },
      focus: () => {
        that.changeContext(context);
      },

      unfocus: () => {
        if (that._currentContext === context) {
          that.changeContext(undefined);
        }
      },

      clearFilters: () => {
        if (that._filterPanel) {
          that._filterPanel.clearFilters();
        } else {
          let keys = Object.keys(model);
          if (!keys || keys.length === 0) return;

          keys.forEach(key => {
            delete model[key];
          });

          _modelChanged.next(model);
        }
      },

      resynch: () => {
        if (context.hasFocus && that._filterPanel) that._filterPanel.resynch();
      },

      get _formlyConfig() {
        if (_formDef) return _formDef;
        if (typeof formlyConfig === 'function') {
          _formDef = formlyConfig();
        } else {
          _formDef = formlyConfig;
        }
        return _formDef;
      },
      _raiseModelChanged: () => {
        _modelChanged.next(model);
      }
    };

    return context;
  }

  /**
   * Used internally by FilterPanelComponent to register with this service.
   */
  private __bindFilterPanel(filterPanel: FilterPanelController) {
    if (this._filterPanel != undefined)
      throw new Error(
        'A FilterPanelComponent has already been bound to the FilterPanelService.'
      );
    this._filterPanel = filterPanel;
    this.changeContext(this._currentContext);
  }

  /**
   * Used internally by FilterPanelComponent to deregister from this service.
   */
  private __unbindFilterPanel(filterPanel: FilterPanelController) {
    if (filterPanel == undefined || filterPanel != this._filterPanel) return;
    this._filterPanel = undefined;
  }

  /**
   * Used internally by FilteredQueryCoordinator so that inheritors do not have to explictly inject global dependencies.
   */
  private __getDependencies(): FilteredQueryDependencies {
    return {
      router: this.router,
      activatedRoute: this.route
    };
  }

  private changeContext(newContext: InternalFilterContext<any>): void {
    this._currentContext = newContext;
    if (!this._filterPanel) return;
    this._filterPanel.applyContext(this.createViewFilterContext());
    if (this.hasContext) {
      this._filterPanel.applyBusyState(this._currentContext.isBusy);
      this._filterPanel.applyDisabledState(this._currentContext.isDisabled);
    }
  }

  private createViewFilterContext(): ViewFilterContext {
    let clientContext = this._currentContext;
    let hasContext = clientContext != undefined;

    let context: ViewFilterContext = {
      isEmpty: !hasContext,
      model: hasContext ? clientContext.model : undefined,
      formlyConfig: hasContext ? clientContext._formlyConfig : undefined,
      raiseModelChanged: () => {
        if (hasContext) {
          clientContext._raiseModelChanged();
        }
      }
    };

    return context;
  }
}
