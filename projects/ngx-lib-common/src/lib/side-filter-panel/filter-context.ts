import { Observable } from 'rxjs';

/**
 * Represents the contextual state of the FilterPanelComponent.
 * A FilterContext encapsulates the form definition used to construct the filter UI component via which the user modifies the filter,
 * exposes the model representing the consequent filter parameters, and exposes methods and properties via which the FilterPanelComponent can be
 * controlled.
 */
export interface FilterContext<TModel> {
  /**
   * A model representing the filter parameters defined by the user through the filter form on the FilterPanelComponent.
   */
  readonly model: TModel;
  /**
   * The event that is raised when the model changes in response to changes made my the user via the filter form on the FilterPanelComponent.
   */
  readonly modelChanged$: Observable<TModel>;
  /**
   * Returns true if this context has one or more filter properties applied, otherwise returns false.
   */
  readonly hasFilter: boolean;
  /**
   * Returns true if this context has focus, otherwise returns false.
   */
  readonly hasFocus: boolean;
  /**
   * Enables the filter form to be programatically configured to enter or exit a busy state for this context.
   */
  isBusy: boolean;
  /**
   * Enables the filter form to be programatically enabled or disabled for this context.
   */
  isDisabled: boolean;
  /**
   * Apply this context to the FilterPanelComponent.
   */
  focus(): void;
  /**
   * Remove this context from the FilterPanelComponent if it is currently applied.
   */
  unfocus(): void;
  /**
   * Clears the filters and model associated with this context.
   */
  clearFilters(): void;
  /**
   * Invoked by client code to refresh the filter panel after the model has been mutated outside of the filter panel UI.
   */
  resynch(): void;
}
