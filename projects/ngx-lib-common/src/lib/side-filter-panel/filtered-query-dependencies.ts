import { Router, ActivatedRoute } from '@angular/router';

export interface FilteredQueryDependencies {
  router: Router;
  activatedRoute: ActivatedRoute;
}
