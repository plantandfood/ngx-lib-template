/**
 * Represents the current page state of a paged dataset.
 */
export interface PageState {
  pageSize: number;
  pageIndex: number;
  pageCount: number;
  resultCount: number;
}
