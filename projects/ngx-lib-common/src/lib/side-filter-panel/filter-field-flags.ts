export enum FilterFieldFlags {
  None = 0x0,
  /**
   * Indicates that the field supports multiple values.
   */
  MultiValue = 0x1
}
