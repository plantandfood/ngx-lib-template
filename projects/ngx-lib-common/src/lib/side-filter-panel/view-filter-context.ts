import { FormlyFieldConfig } from "@ngx-formly/core";

/**
 * Represents the active contextual filter state as it is seen by the FilterPanelComponent.
 */
export interface ViewFilterContext {
    /**
     * Indicates whether or not the active client FilterContext is empty (unassigned - i.e. there is no focused context).
     */
    readonly isEmpty: boolean;
    /**
     * The collection of FormlyFieldConfig instances that define the filter form associated with the active client FilterContext.
     */
    readonly formlyConfig: FormlyFieldConfig[];
    /**
     * The filter model associated with the active client FilterContext.
     */
    readonly model: any;
    /**
     * Raises the modelChanged event on the active client FilterContext.
     */
    raiseModelChanged(): void;
}