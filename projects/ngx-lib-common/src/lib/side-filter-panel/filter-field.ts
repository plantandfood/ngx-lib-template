import { FilterFieldFlags } from './filter-field-flags';

export class FilterField {
  private constructor(
    public readonly key: string,
    public readonly path: string[],
    private flags: FilterFieldFlags = FilterFieldFlags.None
  ) {}

  static create(key: string, flags?: FilterFieldFlags): FilterField {
    return new FilterField(key, key.split('.'), flags);
  }

  get multiValue(): boolean {
    return (
      (this.flags & FilterFieldFlags.MultiValue) === FilterFieldFlags.MultiValue
    );
  }
}
