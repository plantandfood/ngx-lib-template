import { PageState } from './page-state';

/**
 * The dataset returned to a FilteredQueryCoordinator following a filtered query.
 */
export interface FilteredQueryResult<TData> {
  readonly data: TData[];
  readonly page: PageState;
}
