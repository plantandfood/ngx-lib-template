import { Component, Input, Output, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'lib-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.scss']
})
export class SideNavbarComponent {
  @Input()
  flat: Boolean = false;
  @Input()
  links: any;
  @Output()
  expandedParent: string;
  @Output()
  expandedChild: string;
  _element: ElementRef;
  isExpanded = true;

  constructor(public router: Router) {
    window.onresize = () => {
      if (window.innerWidth < 840) {
        this.isExpanded = false;
      } else {
        this.isExpanded = true;
      }
    };

    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.updateUrlProps(event.url);
      }
    });

  }

  updateUrlProps(url) {
    if (url) {
      const parsed = url.split('/');
      if (parsed.length && parsed[1]) {
        this.expandedParent = 'sidenav_link_' + parsed[1].replace(/\-/g, '_');
      } else {
        this.expandedChild = null;
      }
      if (parsed.length && parsed[2]) {
        this.expandedChild = this.expandedParent.concat('_', parsed[2].replace(/\-/g, '_'));
      } else {
        this.expandedChild = null;
      }
    }
  }

  onOpen(parent?: any, child?: any) {
    if (parent) { this.expandedParent = parent; }
    if (child) { this.expandedChild = child; }
  }

  onToggleExpand() {
    this.isExpanded = !this.isExpanded;
  }

}
