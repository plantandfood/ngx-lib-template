export interface NavbarConfig {
  baseUrl: string;
  apiKey: string;
}
