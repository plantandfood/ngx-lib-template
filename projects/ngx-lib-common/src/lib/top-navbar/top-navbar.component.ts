import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Application } from './models/application.model';
import { FilterPanelService } from './../side-filter-panel/filter-panel-service';

@Component({
  selector: 'lib-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.scss']
})
export class TopNavbarComponent  {
  @Input()
  title: string;
  @Input()
  user: any;
  @Output()
  logout: EventEmitter<any> = new EventEmitter();
  @Input()
  applications: Application[];
  @Output()
  toggleFilter: EventEmitter<any> = new EventEmitter();

  constructor(private filterPanelService: FilterPanelService) {}

  onLogout() {
    this.logout.emit();
  }

  navigateToUrl(app) {
    window.open(app.url, '_blank');
  }

  onToggleRightNav() {
    this.toggleFilter.emit();
  }

  get hasFilter(): boolean {
    return this.filterPanelService.hasFilter;
  }

  get hasFilterContext(): boolean {
    return this.filterPanelService.hasContext;
  }

  onToggleFilterPanel() {
    this.filterPanelService.isOpen = !this.filterPanelService.isOpen;
  }
}
