import { Params } from '@angular/router';
import { FormlyFieldConfig } from '@ngx-formly/core';

import { PetsFilterModel } from './pets.component';
import { Injectable } from '@angular/core';
import { DefaultFilteredPageAdapter } from 'projects/ngx-lib-common/src/lib/side-filter-panel/default-filtered-page-adapter';
import { FilterField, FilterFieldFlags, FilteredQueryCoordinator, FilterPanelService,
   FilteredQueryResult } from 'projects/ngx-lib-common/src/lib/side-filter-panel';

@Injectable()
export class PetsQuery extends FilteredQueryCoordinator<
  PetsFilterModel,
  PetsFilterModel,
  any
> {
  constructor(filterPanelService: FilterPanelService) {
    super(filterPanelService, new DefaultFilteredPageAdapter(), [
      FilterField.create('catName', FilterFieldFlags.MultiValue),
      FilterField.create('dogName', FilterFieldFlags.MultiValue),
      FilterField.create('petType'),
      FilterField.create('petColour')
    ]);
  }

  protected executeQuery(params?: Params): Promise<FilteredQueryResult<any>> {
    console.log('*** Executing PetsQuery with params: ', params);
    return new Promise(resolve => {
      setTimeout(() => {
        resolve({
          data: [],
          page: {
            pageSize: 10,
            pageIndex: 0,
            pageCount: 0,
            resultCount: 0
          }
        });
      }, 1000);
    });
  }

  protected createFormDefinition():
    | FormlyFieldConfig[]
    | (() => FormlyFieldConfig[]) {
    return [
      {
        key: 'catName',
        name: 'Cat',
        type: 'select',
        templateOptions: {
          options: [
            { label: 'Smiggles', value: 'smiggles' },
            { label: 'Smiggles2', value: 'smiggles2' }
          ],
          placeholder: 'Choose your favorite Cat',
          multiple: true
        }
      },
      {
        key: 'dogName',
        name: 'Dog',
        type: 'select',
        templateOptions: {
          options: [
            { label: 'Baxter', value: 'baxter' },
            { label: 'Baxter2', value: 'baxter2' }
          ],
          placeholder: 'Choose your favorite Dog',
          multiple: true
        }
      },
      {
        key: 'petType',
        name: 'Pet Type',
        type: 'select',
        templateOptions: {
          options: [
            { label: 'cats', value: 'Cats' },
            { label: 'dogs', value: 'Dogs' },
            { label: 'both', value: 'Cats and Dogs' }
          ],
          placeholder: 'Select pet type',
          multiple: false
        }
      },
      {
        key: 'petColour',
        type: 'input',
        modelOptions: {
          debounce: {
            default: 500
          }
        },
        templateOptions: {
          placeholder: 'Filter by Colour',
          multiple: false
        }
      }
    ];
  }
}
