import { Component, OnDestroy, OnInit } from '@angular/core';
import { PetsQuery } from './pets-query';
import { FilterPanelService } from 'projects/ngx-lib-common/src/lib/side-filter-panel';

export interface PetsFilterModel {
  catName?: string[];
  dogName?: string[];
  petType?: string;
  petColour?: string;
}

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.scss'],
  providers: [PetsQuery]
})
export class PetsComponent implements OnInit, OnDestroy {
  readonly filterService: FilterPanelService;
  constructor(public query: PetsQuery) {
    this.filterService = query.filterPanelService;
  }

  ngOnInit() {
    this.query.init();
  }

  toggleFilterPanel() {
    this.filterService.isOpen = !this.filterService.isOpen;
  }

  toggleDisabled() {
    let filterContext = this.query.filterContext;
    filterContext.isDisabled = !filterContext.isDisabled;
  }

  clearFilters() {
    this.query.filterContext.clearFilters();
  }

  mutateModel() {
    let filterContext = this.query.filterContext;
    let model = filterContext.model;
    if (model.catName && model.catName.length > 0) {
      model.catName = [];
    } else {
      model.catName = ['smiggles'];
    }
    filterContext.resynch();
  }

  ngOnDestroy() {
    this.query.filterContext.unfocus();
  }
}
