import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PetsRoutingModule } from './pets-routing.module';
import { PetsComponent } from './components/pets.component';

@NgModule({
  imports: [CommonModule, PetsRoutingModule],
  declarations: [PetsComponent]
})
export class PetsModule {}
