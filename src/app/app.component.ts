import { Component } from '@angular/core';
import { NavLinks } from './models/nav-link.model';
export const appNavLinks: NavLinks = [
  {
    path: 'pets',
    label: 'Pets',
    ligature: 'spa'
  }
];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styles: ['./app.component.scss']
})
export class AppComponent {
  title = 'Ngx Template App';
  links: NavLinks = appNavLinks;
  user: any = { name: 'Demo', user: 'demo_user' };
  applications: any[] = [{ app_id: 0, title: 'Demo App', url: '/url' }];

}
