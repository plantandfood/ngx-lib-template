export interface NavLink {
  label: string;
  path: string;
  id?: string;
  disabled?: boolean;
  ligature?: string;
  permissions?: string[];
  children?: NavLink[];
}

export type NavLinks = NavLink[];
